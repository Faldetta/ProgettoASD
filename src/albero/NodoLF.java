/*	
 * autori:
 * 		- Chimenti Andrea, 5945877
 * 		- Faldetta Filippo, 5781374
 * 		- Sarti Lorenzo, 5459626
 */

package albero;

import java.util.LinkedList;

public class NodoLF<T> {

	public T info; // informazione del nodo
	public LinkedList<NodoLF<T>> figli; // lista di puntatori ai figli
	public NodoLF<T> padre; // ptr al padre
	public int livello; // livello del nodo

	// costruttore
	public NodoLF(T info) {
		super();
		this.info = info;
		this.figli = new LinkedList<NodoLF<T>>();
	}

	// getters and setters
	public T getInfo() {
		return info;
	}

	public void setInfo(T info) {
		this.info = info;
	}

	public LinkedList<NodoLF<T>> getFigli() {
		return figli;
	}

	public void setFigli(LinkedList<NodoLF<T>> figli) {
		this.figli = figli;
	}

	public NodoLF<T> getPadre() {
		return padre;
	}

	public void setPadre(NodoLF<T> padre) {
		this.padre = padre;
	}

	public int getLivello() {
		return livello;
	}

	public void setLivello(int livello) {
		this.livello = livello;
	}
}