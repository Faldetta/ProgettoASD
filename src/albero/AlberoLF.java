/*	
 * autori:
 * 		- Chimenti Andrea, 5945877
 * 		- Faldetta Filippo, 5781374
 * 		- Sarti Lorenzo, 5459626
 */

package albero;

import java.util.LinkedList;

public class AlberoLF<T> {

	private NodoLF<T> radice; // ptr alla radice
	private int nNodi; // numero di nodi presenti nell'albero
	private int altezza; // altezza dell'albero

	public LinkedList<T> getSonsInfo(NodoLF<T> n) {
		LinkedList<T> l = new LinkedList<>();
		// inserisco nella lista da ritornare le informazioni dei figli del nodo
		for (int i = 0; i < n.getFigli().size(); i++) {
			l.add(n.getFigli().get(i).getInfo());
		}
		return l;
	}

	// cerca un nodo dell'albero date le coordinate, restituisce il nodo trovato
	// o null
	public NodoLF<T> getNodo(String strCoordinate) {
		LinkedList<Integer> cList = getCoordinate(strCoordinate);
		NodoLF<T> tmpNode = radice;
		int i = 0; // scorre lista coordinate
		while (i < cList.size()) {
			tmpNode = tmpNode.getFigli().get(cList.get(i));
			if (tmpNode == null) {
				return null;
			}
			i++;
		}
		return tmpNode;
	}

	// aggiungere una radice in un albero vuoto
	public NodoLF<T> aggiungiRadice(T info) {
		radice = new NodoLF<T>(info);
		nNodi++;
		radice.livello = 0;
		return radice;
	}

	// aggiungere una nuova radice in un albero che ha gia una radice
	public NodoLF<T> aggiungiNuovaRadice(T info) {
		NodoLF<T> v = new NodoLF<T>(info);
		v.getFigli().add(radice);
		radice = v;
		nNodi++;
		altezza++;
		radice.livello = 0;
		incrementaGradi(radice, 0);
		return radice;
	}

	// mettodo per aggiungere un figlio in un dato nodo
	public NodoLF<T> aggiungiFiglio(NodoLF<T> u, T n) {
		NodoLF<T> v = new NodoLF<T>(n);
		u.getFigli().add(v);
		v.padre = u;
		nNodi++;
		v.livello = u.livello + 1;
		if (v.livello > altezza) {
			altezza = v.livello;
		}
		return v;
	}

	// visita in ampiezza (iterativo)
	public LinkedList<T> visitaBFS() {
		LinkedList<NodoLF<T>> frangia = new LinkedList<NodoLF<T>>();
		LinkedList<T> nodiVisitati = new LinkedList<T>();
		frangia.add(radice);
		while (!frangia.isEmpty()) {
			NodoLF<T> estratto = frangia.remove();
			if (estratto != null) {
				nodiVisitati.add(estratto.getInfo());
				for (int i = 0; (estratto.getFigli() != null) && (i < estratto.getFigli().size()); i++) {
					frangia.add(estratto.getFigli().get(i));
				}
			}
		}
		return nodiVisitati;
	}

	// visita in profondita'(iterativo)
	public LinkedList<T> visitaDFS() {
		LinkedList<NodoLF<T>> frangia = new LinkedList<NodoLF<T>>();
		LinkedList<T> nodiVisitati = new LinkedList<T>();
		frangia.push(radice);
		while (!frangia.isEmpty()) {
			NodoLF<T> estratto = frangia.pop();
			if (estratto != null) {
				nodiVisitati.add(estratto.getInfo());
				for (int i = estratto.getFigli().size() - 1; (estratto.getFigli() != null) && (i >= 0); i--) {
					frangia.push(estratto.getFigli().get(i));
				}
			}
		}
		return nodiVisitati;
	}

	// incrementa i gradi dei nodi a parire dalla radice del sottoalbero
	public void incrementaGradi(NodoLF<T> radiceSA, int livello) {
		radiceSA.setLivello(livello);
		for (int i = 0; i < radiceSA.getFigli().size(); i++) {
			incrementaGradi(radiceSA.getFigli().get(i), radiceSA.getLivello() + 1);
		}
	}

	// trasforma una stringa di interi in una lista
	private LinkedList<Integer> getCoordinate(String str) {
		LinkedList<Integer> coo = new LinkedList<Integer>();
		int index = 0;
		while (str != "") {
			index = str.indexOf(','); // cerco posizione della virgola
			if (index == -1) { // caso ultimo numero della stringa
				coo.add(Integer.parseInt(str)); // aggiungo il numero alla lista
				str = "";
			} else { // per tutti gli altri numeri
				coo.add(Integer.parseInt(str.substring(0, index)));
				str = str.substring(index + 1); // tolgo il numero inserito
			}
		}
		return coo;
	}

	// funzione di debug
	public void printVisite() {
		LinkedList<T> l = visitaBFS();
		System.out.print("BFS: ");
		System.out.print("{");
		for (int i = 0; (l != null) && (i < l.size()); i++) {
			System.out.print(" " + l.get(i) + " ");
		}
		System.out.print("}\n");

		l = visitaDFS();
		System.out.print("DFS: ");
		System.out.print("{");
		for (int i = 0; (l != null) && (i < l.size()); i++) {
			System.out.print(" " + l.get(i) + " ");
		}
		System.out.print("}\n");
	}

	// ritorna il padre di un nodo
	public NodoLF<T> getPadre(NodoLF<T> n) {
		return n.getPadre();
	}

	// ritorna l'informazione di un nodo
	public T getInfo(NodoLF<T> n) {
		return n.getInfo();
	}

	// cambia l'informazione di un nodo
	public void setInfo(NodoLF<T> n, T i) {
		n.setInfo(i);
	}

	// ritorna il numero di figli di un nodo
	public int getNumFigli(NodoLF<T> n) {
		return n.getFigli().size();
	}

	// ritorna la radice
	public NodoLF<T> getRadice() {
		return radice;
	}

	// ritorna il numero di nodi
	public int getnNodi() {
		return nNodi;
	}

	// ritorna l'altezza dell'albero
	public int getAltezza() {
		return altezza;
	}
}
