package test;

import albero.AlberoLF;

public class Test {

	public static void main(String[] args) {
		Test t = new Test();
		t.run();
	}

	private void run() {
		AlberoLF<Integer> albero = new AlberoLF<Integer>();
		/* Creo un nuovo albero */
		albero.aggiungiRadice(70);
		albero.aggiungiFiglio(albero.getRadice(), 37);
		albero.aggiungiFiglio(albero.getRadice(), 76);
		albero.aggiungiFiglio(albero.getNodo("0"), 29);
		albero.aggiungiFiglio(albero.getNodo("0"), 56);
		albero.aggiungiFiglio(albero.getNodo("1"), 82);
		albero.aggiungiFiglio(albero.getNodo("0,0"), 21);
		albero.aggiungiFiglio(albero.getNodo("0,0"), 35);
		albero.aggiungiFiglio(albero.getNodo("0,1"), 42);
		albero.aggiungiFiglio(albero.getNodo("0,1"), 60);
		albero.aggiungiFiglio(albero.getNodo("1,0"), 85);
		albero.aggiungiFiglio(albero.getNodo("0,0,0"), 15);
		albero.aggiungiFiglio(albero.getNodo("0,1,0"), 41);
		albero.aggiungiFiglio(albero.getNodo("0,1,0"), 47);
		albero.aggiungiFiglio(albero.getNodo("1,0,0"), 97);

		/* Eseguo le operazioni sull'albero */
		albero.printVisite();
		System.out.println("01) Tot nodi: " + albero.getnNodi());
		System.out.println("02) Numero figli del nodo [0,1]: " + albero.getNumFigli(albero.getNodo("0,1")));
		System.out.println("03) Contenuto del nodo [0,0]: " + albero.getInfo(albero.getNodo("0,0")));
		albero.setInfo(albero.getNodo("0,0"), 33);
		System.out.println("04) Contenuto dopo la modifica: " + albero.getNodo("0,0").getInfo());
		System.out.println("05) Radice dell'albero: " + albero.getRadice().getInfo());
		System.out.println("06) Padre del nodo [0,1,0]: " + albero.getNodo("0,1,0").getPadre().getInfo());
		System.out
				.println("07) Lista dei figli del nodo [0,1]: " + albero.getSonsInfo(albero.getNodo("0,1")).toString());
		System.out.println("09) Inserire una nuova radice nell'albero: ");
		albero.aggiungiNuovaRadice(75);
		albero.printVisite();
		System.out.println("10) Inserisco un nuovo nodo nell'albero: ");
		// albero.aggiungiFiglio(albero.getNodo("0,0,0,0,0"), 14);
		albero.aggiungiFiglio(albero.getNodo("0,0,1"), 99);
		System.out
				.println("Lista dei figli del nodo [0,0,1]: " + albero.getSonsInfo(albero.getNodo("0,0,1")).toString());
		System.out.println("Livello del nodo [0,0,1]: " + albero.getNodo("0,0,1").getLivello());
		albero.printVisite();
		System.out.println("11) Visita di ProfonditÓ: " + albero.visitaDFS().toString());
		System.out.println("12) Visita in Ampiezza: " + albero.visitaBFS().toString());
		System.out.println("13) Altezza dell'albero: " + albero.getAltezza());
	}

}
